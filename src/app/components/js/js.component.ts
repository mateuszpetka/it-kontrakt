import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'app-js',
  templateUrl: './js.component.html',
  styleUrls: ['./js.component.scss']
})
export class JsComponent implements OnInit {
  public elements: string[] = [];
  public removedItem: number;

  constructor() { }

  ngOnInit() {
    this.randomArray(25, 10, 100);
  }

  /**
   * Generuje randomowa tablice
   *
   * @param {number} arrayLength
   * @param {number} minValue
   * @param {number} maxValue
   */
  randomArray(arrayLength: number, minValue: number, maxValue: number): void {
    this.elements = new Array(arrayLength)
      .fill(0)
      .map((n) => {
        const randomValue: number = Math.random() * (maxValue - minValue) + minValue;
        return randomValue.toFixed(2);
      });
  }

  /**
   * Usuwa element z tablicy
   *
   * @param {number} i
   */
  deleteItemFromArray(i: number): void {
    _.remove(this.elements, (item, index) => {
      return index === i;
    });

    this.removedItem = i;
  }

  /**
   * Zaznacza co trzeci element i dodaje klase
   *
   * @param {HTMLElement} e
   * @param {number} i
   */
  selectThirdElement(e: HTMLElement, i: number): void {
    if (i % 3 === 1) {
      e.classList.toggle('item--transform');
    }
  }

}
