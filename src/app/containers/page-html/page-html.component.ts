import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-html',
  templateUrl: './page-html.component.html',
  styleUrls: ['./page-html.component.scss']
})
export class PageHtmlComponent implements OnInit {
  public visibleLeftSidebar: boolean = true;
  public visibleRightSidebar: boolean = true;

  constructor() { }

  ngOnInit() {
    this.visibleLeftSidebarState(event);
    this.visibleRightSidebarState(event);
  }


  visibleLeftSidebarState(event): void {
    this.visibleLeftSidebar = event;
  }

  visibleRightSidebarState(event): void {
    this.visibleRightSidebar = event;
  }
}
