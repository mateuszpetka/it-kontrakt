import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { JsComponent } from './components/js/js.component';
import { LeftSidebarComponent } from './components/html/left-sidebar/left-sidebar.component';
import { RightSidebarComponent } from './components/html/right-sidebar/right-sidebar.component';
import { MainComponent } from './components/html/main/main.component';
import { HeaderComponent } from './components/html/header/header.component';
import { FooterComponent } from './components/html/footer/footer.component';
import { PageJsComponent } from './containers/page-js/page-js.component';
import { PageHtmlComponent } from './containers/page-html/page-html.component';

@NgModule({
  declarations: [
    AppComponent,
    JsComponent,
    LeftSidebarComponent,
    RightSidebarComponent,
    MainComponent,
    HeaderComponent,
    FooterComponent,
    PageJsComponent,
    PageHtmlComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
