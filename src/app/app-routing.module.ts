import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {PageJsComponent} from './containers/page-js/page-js.component';
import {PageHtmlComponent} from './containers/page-html/page-html.component';


const routes: Routes = [
  {
    path: 'page-js',
    component: PageJsComponent
  },
  {
    path: 'page-html',
    component: PageHtmlComponent
  },
  { path: '',
    redirectTo: '/page-js',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
