import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageJsComponent } from './page-js.component';

describe('PageJsComponent', () => {
  let component: PageJsComponent;
  let fixture: ComponentFixture<PageJsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageJsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageJsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
